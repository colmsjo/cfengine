Configuration Management for gizur.com
=====================================



This repository contains the configuration for all servers having custom setup. All configuration is applied using 
the open source configuration managmenet solution cfengine, see http://cfengine.com/


Vagrant
-------

NOTE: Migration to vagrant is 'work in progress'

Bring up the virtual machin with `vagrant up` and login with `vagrant ssh` (as usual).

```
cp /vagrant/cfengine/bootstrap-vagrant.sh .
chmod +x bootstrap-vagrant.sh 
 ./bootstrap-vagrant.sh 
...
/var/lib/cfengine3/cf_classes.db: No such file or directory
/var/lib/cfengine3/state/cf_lock.db: No such file or directory
/var/lib/cfengine3/performance.db: No such file or directory

# Workaround: If the scripts halts here (check what processes that are running using top
# in a separate shell), press Ctrl+C. Check again i top that processes are executing
 

# Set the roles for this server, for exmaple CFEXECD_ARGS="--define deployment"
sudo nano /etc/default/cfengine3

sudo service cfengine3 restart

```


Ubuntu
------

First copy the SSH keys for the deployment role to the new server (assuming ubuntu 12.04):
```
# Login as superadmin to a server implementing the deployment roles
cd ~/cfengine
scp -i ~/intern/keys/gc1-keypair(1|2|3|4).pem roles/generic/root/.ssh/id_rsa*  ubuntu@XXX.XXX.XXX.XXX:~/.ssh
```

## Alt. 1 - bootstrap using script

```
scp -i ~/intern/keys/gc(1|2|3|4)-keypair1.pem bootstrap.sh  ubuntu@XXX.XXX.XXX.XXX:~/

# Login to the new server
ssh -i ~/intern/keys/gc(1|2|3|4)-keypair.pem ubuntu@XXX.XXX.XXX.XXX

# Run bootstrap.sh
chmod +x bootstrap.sh

# Answer yes on the question: Are you sure you want to continue connecting (yes/no)? yes
./bootstrap.sh
...
/var/lib/cfengine3/cf_classes.db: No such file or directory
/var/lib/cfengine3/state/cf_lock.db: No such file or directory
/var/lib/cfengine3/performance.db: No such file or directory

# Workaround: If the scripts halts here (check what processes that are running using top
# in a separate shell), press Ctrl+C. Check again i top that processes are executing
 

# Set the roles for this server, for exmaple CFEXECD_ARGS="--define git_server --define rabbitmq --define php_batches --define interface_server"
sudo nano /etc/default/cfengine3

sudo service cfengine3 restart
```


Test that things install ok using: `sudo cf-agent --verbose`


## Alt 2. - bootstrap manually

Then login to the server and install cfengine and git:
```
ssh -i ~/intern/keys/gc1-keypair(1|2|3|4).pem ubuntu@XXX.XXX.XXX.XXX

# Fix permissions
chmod 400 .ssh/id_rsa*

# Copy the deployment SSH keys to the root account
sudo cp .ssh/id_rsa* /root/.ssh

sudo apt-get install -y cfengine3 git
```

Then this repository needs to be cloned using git:
```
cd /etc/cfengine3
sudo git clone git@bitbucket.org:gizurinc/cfengine .
sudo chmod -R og=r /etc/cfengine3/roles
```

Update the cfengine configuration:
```
# Set the roles to be used, for instance CFEXECD_ARGS="--define git_server --define rabbitmq --define php_batches --define interface_server"
# Also enable appropriate daemons, typically RUN_CFEXECD=1
sudo nano /etc/default/cfengine3

# Start cfengine
sudo /etc/init.d/cfengine3 restart
```


Postfix configuration
---------------------

The following needs to be performed manually in order to make it possible to send mails from other hosts than localhost

```
nano /etc/postfix/main.cf


...
smtpd_use_tls=no
...

mynetworks = 10.0.0.0/8 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
...


```


Amazon Linux / redhat / centos : BETA
-------------------------------------


First copy the SSH keys for the deployment role to the new server (assuming ubuntu 12.04):
```
# Login as superadmin to a server implementing the deployment roles
cd ~/cfengine
scp -i ~/intern/keys/gc(1|2|3|4)-keypair1.pem roles/generic/root/.ssh/id_rsa*  ec2-user@XXX.XXX.XXX.XXX:~/.ssh
```


## bootstrap manually (there is no script)

Then login to the server and install cfengine and git:
```
ssh -i ~/intern/keys/gc1-keypair(1|2|3|4).pem ec2-user@XXX.XXX.XXX.XXX

# Fix permissions
chmod 400 .ssh/id_rsa*

# Copy the deployment SSH keys to the root account
sudo cp .ssh/id_rsa* /root/.ssh

wget http://cfengine.com/pub/gpg.key
sudo rpm --import gpg.key
rm gpg.key

sudo nano /etc/yum.repos.d/cfengine-community.repo

[cfengine-repository]
name=CFEngine
baseurl=http://cfengine.com/pub/yum/
enabled=1
gpgcheck=1
 

# Install the package
sudo yum -y install cfengine-community
```

Then this repository needs to be cloned using git:
```
cd /var/cfengine
sudo git clone git@bitbucket.org:gizurinc/cfengine inputs
sudo chmod -R og=r /var/cfengine/inputs/roles
```

Update the cfengine configuration:
```
# Set the roles to be used, for instance CFEXECD_ARGS="--define git_server --define rabbitmq --define php_batches --define interface_server"
# Also enable appropriate daemons, typically RUN_CFEXECD=1
sudo nano /etc/sysconfig/cfengine3

# Start cfengine
sudo service cfengine3 restart
```


Some permissions needs to ba changed in order to make it possible to test things:
```
cd /var/cfengine
sudo chmod a+x inputs bin
cd inputs
sudo ../bin/cf-agent --verbose --define ebeanstalk

```

