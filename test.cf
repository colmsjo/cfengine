body common control {

    any::
        bundlesequence => {
            "deployment"
        };

        inputs => {
            "cfengine_stdlib.cf",
        };
}


#######################
#  Main agent bundle
#

bundle agent deployment {

classes:
    "deployment_ubuntu" expression => "deployment & ubuntu";


#
# Install packages

vars:
    "match_package" slist => { 
                          "ruby", 
                          };


packages:

    #
    # Ubuntu uses apt
    #

    deployment_ubuntu::

        "$(match_package)"

            package_policy => "addupdate",
            package_method => apt,
            handle         => "packages";


#
# Set the server role
#

files:

    deployment::

        "/etc/environment"
             edit_line     => insert_lines('GC_ROLE="DEPLOYMENT"'),
             comment       => "Set the server role to deployment";


#
# Setup users
#

vars:
    
    any::

        "userlist" slist => { "cloud1", "cloud2", "cloud3", "cloud4"  };


methods:
 
    any::

        "any" usebundle => gcCreateUser;

        "any" usebundle => gcSetupAWS("$(userlist)");

        "any" usebundle => gcSetupUser("$(userlist)");

}


#######################
#  Create the users
#

bundle agent gcCreateUser {
    
classes:
    "deployment_ubuntu" expression => "deployment & ubuntu";


commands:

 
    deployment_ubuntu::

       "/usr/bin/sudo sh -c 'useradd -D -s /bin/bash'"
            handle         => "useradd_set_defaults",
            comment        => "Set password for cloud users";

        "/usr/bin/sudo sh -c '/usr/sbin/useradd -m cloud1 && /usr/sbin/useradd -m cloud2 && /usr/sbin/useradd -m cloud3 && /usr/sbin/useradd -m cloud4'"
            handle         => "add_cloud_user",
            depends_on     => { "useradd_set_defaults" },
            comment        => "Create cloud user";

        "/usr/bin/sudo sh -c '/usr/sbin/chpasswd < $(g.config_input_dir)/roles/deployment/passwd'"
            depends_on     => { "add_cloud_user" },
            handle         => "passwd_cloud_user",
            comment        => "Set password for cloud users";


}


#######################
# Fix permissions etc.
#

bundle agent gcSetupUser(user) {


#
# Setup SSH
#

files:

    any::

        "/home/$(user)/.ssh/"
            create    => "true",
            perms     => mog("600", "$(user)", "$(user)"),
            comment   => "Create .ssh directory for $(user)";

        "/home/$(user)/.ssh/id_rsa"
            copy_from => local_cp("$(g.config_input_dir)/roles/deployment/home/template_user/.ssh/id_rsa"),
            perms     => mog("600", "$(user)", "$(user)"),
            comment   => "Copy private SSH key file for $(user)";

        "/home/$(user)/.ssh/id_rsa.pub"
            copy_from => local_cp("$(g.config_input_dir)/roles/deployment/home/template_user/.ssh/id_rsa.pub"),
            perms     => mog("600", "$(user)", "$(user)"),
            comment   => "Copy public SSH key file for $(user)";

        "/home/$(user)/.ssh/authorized_keys"
            perms        => mog("600", "$(user)", "$(user)"),
            comment      => "Fix permissions for authorized_keys for $(user)";


commands:

    any::

        "/usr/bin/sudo sh -c '/bin/cat $(g.config_input_dir)/roles/deployment/home/$(user)/.ssh/*.pub > /home/$(user)/.ssh/authorized_keys'"
            comment        => "Create authorized_keys for $(user)";


#
# Setup AWS
#

files:

    any::

        "/home/$(user)/.bashrc"
            edit_line     => insert_lines("export AWS_CREDENTIAL_FILE=~/credentials"),
            comment       => "Set AWS_CREDENTIAL_FILE for $(user)";

        "/home/$(user)/.bashrc"
            edit_line     => insert_lines("export ELASTICBEANSTALK_URL=https://elasticbeanstalk.eu-west-1.amazonaws.com"),
            comment       => "Set ELASTICBEANSTALK_URL for $(user)";

        "/home/$(user)/.bashrc"
            edit_line     => insert_lines("export PATH=$PATH:~/local/AWS-ElasticBeanstalk-CLI-2.0/eb/linux/python2.7"),
            comment       => "Set PATH for $(user)";

        "/home/$(user)/credentials"
            copy_from => local_cp("$(g.config_input_dir)/roles/deployment/home/$(user)/credentials"),
            perms     => mog("600", "$(user)", "$(user)"),
            comment   => "Copy aws credentials file for $(user)";

       "/home/$(user)/gizurcloud"
            perms        => mog("644", "$(user)", "$(user)"),
            handle       => "fix_gizurcloud_permissions",
            comment      => "Fix permissions for gizurcloud for $(user)";

        "/home/$(user)/gizurcloud/"
            perms        => mog("644", "$(user)", "$(user)"),
            depth_search => recurse("inf"),
            handle       => "fix_gizurcloud_permissions2",
            comment      => "Fix permissions for gizurcloud for $(user)";

        "/home/$(user)/local/"
            create    => "true",
            perms     => mog("644", "$(user)", "$(user)"),
            comment   => "Create local directory for $(user)";

        "/home/$(user)/local/AWS-ElasticBeanstalk-CLI-2.0/eb/linux/python2.7/eb"
            perms        => mog("700", "$(user)", "$(user)"),
            comment      => "Fix permissions for eb for $(user)";


}


#######################
# Setup repositories and unpack zip files 

bundle agent gcSetupAWS(user) {

classes:

    "aws_not_there"             not => fileexists("/home/$(user)/local/AWS-ElasticBeanstalk-CLI-2.0");

    "gizurcloud_repo_not_there" not => fileexists("/home/$(user)/gizurcloud");

commands:

    aws_not_there::
        "/usr/bin/unzip"
            args => "-d /home/$(user)/local $(g.config_input_dir)/roles/deployment/install/AWS-ElasticBeanstalk-CLI-2.0.zip",
            comment       => "Unzipping AWS Cli for $(user)",
            handle        => "copy_aws_cli_zip_to_user";

    gizurcloud_repo_not_there::
        "/usr/bin/git"
            chdir         => "/home/$(user)",
            args          => "clone git@github.com:gizur/gizurcloud.git",
            handle        => "clone_gizurcloud",
            comment       => "Cloning the gizurcloud repo to $(user)";

    any::
 
        "/bin/sh"
            chdir         => "/home/$(user)/gizurcloud",
            args          => "/home/$(user)/local/AWS-ElasticBeanstalk-CLI-2.0/AWSDevTools/Linux/AWSDevTools-RepositorySetup.sh",
            depends_on    => { "clone_gizurcloud" };   

}



# global vars
bundle common g {
 
vars:
 
    # This needs to be chnaged according to the installtion
    "workdir"           string => "/etc/cfengine3";

    "config_input_dir"  string => "$(workdir)";
}


