#!/bin/bash
#
# 120917, Jonas COlmsjö
#
# Bootstrap a server with cfengine and the generic role
#

echo "This will take a while, please be patient..."

# Fix permissions
chmod 400 .ssh/id_rsa*

# Copy the deployment SSH keys to the root account
sudo cp .ssh/id_rsa* /root/.ssh

# Install cfengine and git client
sudo apt-get install -y cfengine3 git

# Pull the cfengine configuration
cd /etc/cfengine3
sudo git clone git@bitbucket.org:gizurinc/cfengine .
sudo chmod -R og=r /etc/cfengine3/roles

# Enable cf-execd daemon
sudo sh -c "cat /etc/default/cfengine3 | sed s/'RUN_CFEXECD=0'/'RUN_CFEXECD=1'/ > /etc/default/cfengine3.new"
sudo mv /etc/default/cfengine3.new /etc/default/cfengine3

# Run cf-engine a first time
sudo sh -c "cf-agent --verbose > cfengine-bootstrap.txt"

# Start cfengine
sudo /etc/init.d/cfengine3 restart


echo "NOTE: Now the roles for this specific server needs be configured in /etc/default/cfengine3..."
